module.exports = {
  // fix safari stale cache bug
  devServer: {
    before: app => {
      app.set("etag", "strong");
      app.use((req, res, next) => {
        res.set("Cache-Control", "must-revalidate");
        next();
      });
    }
  }
};
