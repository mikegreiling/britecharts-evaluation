export default function generateData() {
  const names = [
    "Shiny",
    "Blazing",
    "Dazzling",
    "Radiant",
    "Sparkling",
    "Other"
  ];
  const values = [];
  let sum = 0;
  let id = 0;

  names.forEach(name => {
    const quantity = Math.floor(Math.random() * 500);
    sum += quantity;
    id += 1;
    values.push({ name, id, quantity });
  });

  values.forEach(value => {
    value.percentage = Math.round((value.quantity / sum) * 100);
  });

  return values;
}
